package edu.miracosta.cs113.hw11;
import java.util.*;

public class ShakerShort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] shake = new int[]{5,12,3,4,50,17,1};
		System.out.println("        Shaker Short");
		System.out.println("        -------------");
		System.out.println("");
		System.out.println("Before Sorting:");
		for(int i = 0; i < shake.length;i++){
			System.out.println(shake[i]);
		}
		shakerShort(shake);
		System.out.println("==============");
		System.out.println("After Sorting:");
		for(int i = 0; i < shake.length;i++){
			System.out.println(shake[i]);
		}
	}
    public static void shakerShort(int[] shake){
    	boolean swapped = true;
        int i = 0;
        int j = shake.length - 1;
        while(i < j && swapped) 
        {
           swapped = false;
           for(int k = i; k < j; k++) 
           {
              if(shake[k] > shake[k + 1]) 
              {
                 int temp = shake[k];
                 shake[k] = shake[k + 1];
                 shake[k + 1] = temp;
                 swapped = true;
              }
           }
           j--;
           if(swapped) 
           {
              swapped = false;
              for(int k = j; k > i; k--) 
              {
                 if(shake[k] < shake[k - 1]) 
                 {
                    int temp = shake[k];
                    shake[k] = shake[k - 1];
                    shake[k - 1] = temp;
                    swapped = true;
                 }
              }
           }
           i++;
        }
    }
}
