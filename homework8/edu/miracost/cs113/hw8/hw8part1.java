package edu.miracost.cs113.hw8;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;

public class hw8part1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//ArrayList
		 LinkedList<String> listCh = new  LinkedList<String>();
		 LinkedList<Integer> counts = new  LinkedList<Integer>();
		 
	     File inFile = new File("readFile.txt");
	     try {
			Scanner file = new Scanner(inFile);
		
	     StringTokenizer tokenizer = new StringTokenizer("readFile.txt", " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\n\t");
		 while (tokenizer.hasMoreTokens()){
		 int count = 0;
		 String ch = tokenizer.nextToken();
		 for(String c : listCh){
			 // this will check the linklist list if the char is in their. 
			 // and if it is it will get the spot in the String list then 
			 // in the same equal spot in another list it will get the number and add 1
			 if(ch == c){
				 int b = counts.get(count);
				 b+=1;
				 counts.add(count,b);
			 }
			 // if the character is not in the first list then it will add for future use later on. 
			 else{
				 // adds to end off list and count adds 1 
				 listCh.add(ch);
				 int b = counts.get(counts.size()-1);
				 b+=1;
				 counts.add(count,b);
			 }
		  } 
		 }
	    }
	     catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }		 
	 System.out.println(listCh);
	 System.out.println(counts);
	 
  }

}
