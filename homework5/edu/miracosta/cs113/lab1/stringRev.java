package edu.miracosta.cs113.lab1;

import java.util.Stack;

public class stringRev 
{
	static String inputString = "doodle";
	String result;
	static Stack<Character> charStack = new Stack<Character>();
	
	public static void main(String[] args) 
	{
	  fillStack();
	  buildReverse();
	 
	}
	public static void fillStack()
	{
		for(int i = 0; i < inputString.length(); i++){
			charStack.push(inputString.charAt(i));
		}
	}
	public static String buildReverse(){
		StringBuilder result = new StringBuilder();
		while(!charStack.empty()){
			result.append(charStack.pop());
		}
		System.out.println(result);
		return result.toString();
	}
}
