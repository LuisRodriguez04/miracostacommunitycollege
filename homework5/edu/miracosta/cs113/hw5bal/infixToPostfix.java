package edu.miracosta.cs113.hw5bal;
import java.util.*;

public class infixToPostfix {
	private Stack<Character> operatorSt; 
	private static final String operators = "+-*/()[]{}";
	private static final int[] precendences = {1,1,2,2,-1,-1};
	private StringBuilder postfix;
	
	public String convert(String infix) throws SyntaxErrorException {
		operatorSt = new Stack<Character>();
		postfix = new StringBuilder();
		try{
			String nxt;
			Scanner key = new Scanner(infix);
			while((nxt = key.findInLine(infix))!= null){
				char firstChar = nxt.charAt(0);
				if(Character.isJavaIdentifierStart(firstChar)|| Character.isDigit(firstChar)){
					postfix.append(nxt);
					postfix.append(' ');
				}
				else if(isOperator(firstChar)){
					operatorProc(firstChar);
				}
				else{
					throw new SyntaxErrorException ("Error" + firstChar);
				}
			}
			while(!operatorSt.empty()){
				char op = operatorSt.pop();
				if(op== '(')
					throw new SyntaxErrorException ("Error");
				postfix.append(op);
				postfix.append(' ');
			}
			return postfix.toString();
		}
		catch(EmptyStackException e){
			throw new SyntaxErrorException("Stack is empty");
			
		}
	}
	private void operatorProc(char op){
		if(operatorSt.empty() || op == '(' ){
			operatorSt.push(op);
		}else{
			char topOp = operatorSt.peek();
			if(precendence(op) > precendence(topOp)){
				operatorSt.push(op);
			}
			else{
				while(!operatorSt.empty() && precendence(op) <= precendence(topOp)){
					operatorSt.pop();
					postfix.append(topOp);
					postfix.append(' ');
					if(!operatorSt.empty()){
						topOp = operatorSt.peek();
					}
				}
				operatorSt.push(op);
			}
		}
	}
	private boolean isOperator(char ch){
		return operators.indexOf(ch) != -1;
	}
	private int precendence(char op){
		return precendences[operators.indexOf(op)];
	}
	public static class SyntaxErrorException extends Exception
	{
	 SyntaxErrorException(String message){
		 super(message);
	 }
	}
}
