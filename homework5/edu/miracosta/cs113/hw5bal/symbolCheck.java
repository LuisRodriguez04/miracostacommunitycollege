package edu.miracosta.cs113.hw5bal;

import java.util.*;

import edu.miracosta.cs113.hw5bal.infixToPostfix.SyntaxErrorException;

public class symbolCheck 
{
	public static void main(String[] args) throws SyntaxErrorException
	{
		Scanner keyboard = new Scanner(System.in);
		infixToPostfix obj = new infixToPostfix();
		parenthChecker obj2 = new parenthChecker();
		postToInfix obj3 = new postToInfix();
		String equation;
		int choice;
		System.out.println("This will check if your equation is balanced or not.");
		System.out.println("You can enter an equation with any operation sign");
		System.out.println("'+' - adding\n" + "'-' - subtraction\n" + "'*' - multiplication\n" + "'/' - division\n" + "'%' - modulus");
		System.out.println(" ");
		System.out.println("1- infix to postfix\n2-postfix to infix");
		System.out.println("Enter your equation: ");
		equation = keyboard.nextLine();
		System.out.println("Enter choice");
		choice =keyboard.nextInt();
		if(choice == 1){
		    obj.convert(equation);
			obj2.isBalanced(equation);
			if(obj2.isBalanced(equation) == true)
			{
			  System.out.println("equation balanced");
			}
			else{
			 System.out.println("Not balanced");
			}
		}
	    if (choice == 2){
	    	try {
				obj3.eval(equation);
			} catch (edu.miracosta.cs113.hw5bal.postToInfix.SyntaxErrorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			obj2.isBalanced(equation);
			if(obj2.isBalanced(equation) == true)
			{
			  System.out.println("equation balanced");
			}
			else{
			 System.out.println("Not balanced");
			}
	    }
		
	}
	
}
