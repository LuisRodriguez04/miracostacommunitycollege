package edu.miracosta.cs113.hw5bal;

import java.util.Stack;
import java.util.EmptyStackException;

public class parenthChecker 
{
	private static final String open = "([{";
	private static final String closed = ")]}";
	
	public static boolean isBalanced(String equation)
	{
	  Stack <Character> s = new Stack<Character>();
	  boolean balanced = true;
	  try
	  {
	    int index = 0;
	    while(balanced && index < equation.length())
	    {
		 char nextChar = equation.charAt(index);
		 if(isOpen(nextChar))
		 {
		  s.push(nextChar);	 
		 }
		 else if(isClose(nextChar))
		 {
		  char topCh = s.pop();
		  balanced = open.indexOf(topCh) == closed.indexOf(nextChar);
		 }
		  index++;
	    }
	  }
	  catch(EmptyStackException e)
	  {
		  balanced = false;
	  }
	  return (balanced && s.empty());
	}
	private static boolean isOpen(char ch)
	{
		return open.indexOf(ch) > -1;
	}
	private static boolean isClose(char ch)
	{
		return closed.indexOf(ch) > -1;
	}
	

}
