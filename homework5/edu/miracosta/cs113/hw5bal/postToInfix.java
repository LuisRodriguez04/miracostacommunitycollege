package edu.miracosta.cs113.hw5bal;
import java.util.*;

public class postToInfix {

	public static class SyntaxErrorException extends Exception
	{
	 SyntaxErrorException(String message){
		 super(message);
	 }
	}
	private static final String operator = "+-*/";
	private static Stack<Integer> operandStack;
	
	private static int evalOp(char op){
		int rhs = operandStack.pop();
		int lhs = operandStack.pop();
		int result = 0;
		switch(op){
			case'+' : result = lhs + rhs;
				break;
			case'-' : result = lhs - rhs;
				break;
			case'/' : result = lhs / rhs;
				break;
			case'*' : result = lhs * rhs;
				break;
		}
		return result;
	}
	
	private static boolean isOperator(char ch){
		return operator.indexOf(ch) != -1;
	}
	public static int eval(String expression) throws SyntaxErrorException{
		operandStack = new Stack<Integer>();
		String[] tokens = expression.split(expression);
		try{
			for(String nextToken: tokens){
				char firstChar = nextToken.charAt(0);
				if(Character.isDigit(firstChar)){
					int value = Integer.parseInt(nextToken);
					operandStack.push(value);
				}else if(isOperator(firstChar)){
					int result = evalOp(firstChar);
					operandStack.push(result);
				}
				else{
					throw new SyntaxErrorException("Invalidchar"+ firstChar);
				}
			}
			int answer = operandStack.pop();
			if(operandStack.empty()){
				return answer;
			}else{
				throw new SyntaxErrorException("stack should be empty");
			}
		}catch(EmptyStackException e){
			throw new SyntaxErrorException("stack should be empty");
		}
	}
}
