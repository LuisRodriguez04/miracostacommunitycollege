// Luis R. Bijan V.
		
/* Self-Check 1 pg. 366: 
    -Create Hashset named s containing data type String
    -adds String hello to set but checks if it is in the set before adding
    -adds String bye to set but checks if it is in the set before adding
    -for addAll it wont have any affect 
    -create new set T contains data type string 
    -adds String "123" to set T.
    -adds String "123" to set S
    -1st print is True 
    -2nd print is false
    -3rd print is false
    -4th print is true
    -5th print is true
    -6th print is true
    -will not do anything
    -will print out true.
*/
		
/*programming part 1:
   a. setC = SetA
      setC.addAll(setB);

   b. setC = SetA
      setC.retainAll(setB);

   c. setC = SetA
      setC.removeAll(setB);

   d. if(setA.containAll(setB))
         setC = SetA;  
      else
	 setC = SetB;
*/
		
/*programming part 2:
    StringBuilder list = new StringBuilder();
    for( <E> item : set)
	 list.append(item);
    return list.toString();
*/