package edu.miracosta.cs113.lab1CircLink;

import java.util.*;

public class MyQueue<E> {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner key = new Scanner(System.in);
		Queue<String> names = new LinkedList<String>();
		boolean ex = true;
		names.offer("Joe");
		names.offer("Luis");
		names.offer("Bob");
		names.offer("Jose");
		names.offer("Timmy");
		names.offer("Dude");
		while(ex){
		System.out.println("What would you like to do.");
		System.out.println("1- peek\n2- remove front\n3- poll\n4- element\n5- offer\n6- show list\n7- Exit");
		System.out.println();
		System.out.println("Choice");
		int choice = key.nextInt();
		if(choice == 1){
			System.out.println(peek(names));
			System.out.println("");
		}else if (choice == 2){
			System.out.println(remove(names)+": Was removed from list.");
			System.out.println("");
		}else if(choice == 3){
			System.out.println(poll(names)+": Was removed from list.");
			System.out.println("");
		}else if(choice == 4){
			element(names);
			System.out.println("");
		}
		 else if(choice == 5){
			System.out.println("Enter new Name");
			String newName = key.next();
			System.out.println(offer(names, newName));
			System.out.println("");
		}else if (choice == 6){
			for(String list : names){
			System.out.println(list);
			System.out.println("");
			}
		}else if(choice == 7){
			ex = false;
		}
	 }	
  }
	private static boolean offer(Queue names,String newName){
		if(names.offer(newName)){
			return true;
		}
		else
			return false;
	}
	private static Object peek(Queue names){
		if(names.isEmpty()){
			return null;
		}
		else{
			return ((LinkedList) names).get(0);
			
		}
	}
	private static Object remove(Queue names){
		if(names.isEmpty()){
			throw new NoSuchElementException();
		}
		else
			return ((LinkedList) names).remove(0);
	}
	private static Object poll(Queue names){
		if(names.isEmpty()){
			return null;
		}
		else{
			return ((LinkedList) names).remove(0);
			
		}
	}
	private static Object element(Queue names){
		if(names.isEmpty()){
			throw new NoSuchElementException();
		}
		else
			return ((LinkedList) names).get(0);
	}
}
