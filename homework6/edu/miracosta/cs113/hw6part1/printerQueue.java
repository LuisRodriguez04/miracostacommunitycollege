package edu.miracosta.cs113.hw6part1;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class printerQueue {
	static double totalTime=0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random nextNum = new Random();
		Scanner in = new Scanner(System.in);
		
		
		Queue<Integer> queue = new LinkedList<Integer>();
		Queue<Integer> nowPrint = new LinkedList<Integer>();
		Queue<Integer> nowPrint2 = new LinkedList<Integer>();
		Queue<Integer> nowPrint3 = new LinkedList<Integer>();
		adding obj2 = new adding();
		
		int counter = 0;
		
		// Size of job depends on number
		// numbers will range from 1-50
		// 1-being the smallest and 50- the largest.
		// 10 pages per second in this case.
		queue.offer(50);
		queue.offer(28);
		queue.offer(30);
		queue.offer(40);
		System.out.println("How many printers would you like to choose"
				+ "\n1-For 1 printer\n2-For 2 printers\n3-For 3 printers");
		System.out.println();
		System.out.println("Choice: ");
		int choice = in.nextInt();
		if(choice == 1){
			ChoiceOne(queue, nowPrint, nextNum, counter);
		}
		if(choice == 2){
			ChoiceTwo(queue, nowPrint,nowPrint2, nextNum, counter);
		}
		if(choice == 3){
			ChoiceThree(queue, nowPrint,nowPrint2,nowPrint3, nextNum, counter);
		}	
}// end main 	
   //choosing one printer
   public static void ChoiceOne(Queue queue, Queue nowPrint, Random nextNum, int counter)
   {
	   adding obj = new adding();
	   Timer timer = new Timer();
	   
	   // adds job every 2 seconds calls method adding
	   timer.schedule(new adding(), 0, 2000);
	   if(counter < 4){
		   int random = nextNum.nextInt(50) + 1;
			queue.offer(random);
			}
		while(nowPrint.size()<1){
			Collections.sort((List<Integer>) queue, null);  
			priority(queue, nowPrint);
			counter++;
			if(obj.getCounter()<=3){
				// cancels timer when counter or jobs added is 3
				timer.cancel();
			}
		}
		if(queue.isEmpty()){
			System.out.println("Your queue is now empty no more"
					+ " print jobs are available.");	
		}
   }
   public static void ChoiceTwo(Queue queue, Queue nowPrint,Queue nowPrint2, Random nextNum, int counter)
   {
	   adding obj = new adding();
		while((nowPrint.size()<1 && nowPrint2.size()<1) && counter < 20){
			if(counter < 3){
				int random = nextNum.nextInt(50) + 1;
				queue.offer(random);
			}
			Collections.sort((List<Integer>) queue, null);  
			priority2(queue, nowPrint,nowPrint2);
			counter++;
		}
		if(queue.isEmpty()){
			System.out.println("Your queue is now empty no more"
					+ " print jobs are available.");	
		}
   }
   public static void ChoiceThree(Queue queue, Queue nowPrint,Queue nowPrint2,Queue nowPrint3, Random nextNum, int counter)
   {
	   adding obj = new adding();
		while((!queue.isEmpty() && nowPrint.size()<1 && nowPrint2.size()<1 && nowPrint3.size()<1) || counter < 20){
			if(counter < 4){
			int random = nextNum.nextInt(50) + 1;
			queue.offer(random);
			}
			Collections.sort((List<Integer>) queue, null);  
			priority3(queue, nowPrint,nowPrint2,nowPrint3);
			counter++;
		}
		if(queue.isEmpty()){
			System.out.println("Your queue is now empty no more"
					+ " print jobs are available.");	
		}
    }
	public static void priority(Queue queue, Queue<Integer> nowPrint){
		int transfer;
		if(nowPrint.isEmpty() && nowPrint.size()<1){
			if(!queue.isEmpty()){
			transfer = (int) queue.peek();
			queue.poll();
			nowPrint.offer(transfer);
			if(transfer > 0){
				nowPrinting(nowPrint);
			}
		  }
		}
	}
	public static void priority2(Queue queue,Queue<Integer> nowPrint,  Queue<Integer> nowPrint2){
		int transfer1;
		int transfer2;
		if(nowPrint.isEmpty() && nowPrint.size()<1 && !queue.isEmpty()){
			
			transfer1 = (int) queue.peek();
			queue.poll();
			nowPrint.offer(transfer1);
			if(transfer1 > 0){
				nowPrinting(nowPrint);
			}
		if(nowPrint2.isEmpty() && nowPrint2.size()<1 && !queue.isEmpty()){
				
				transfer2 = (int) queue.peek();
				queue.poll();
				nowPrint2.offer(transfer2);
				if(transfer2 > 0){
					nowPrinting2(nowPrint2);
				 }
		  }
	  }
	}
	public static void priority3(Queue queue,Queue<Integer> nowPrint,  Queue<Integer> nowPrint2, Queue<Integer> nowPrint3){
		int transfer1;
		int transfer2;
		int transfer3;
		if(nowPrint.isEmpty() && nowPrint.size()<1 ){
			if(!queue.isEmpty()){
			transfer1 = (int) queue.peek();
			queue.poll();
			nowPrint.offer(transfer1);
			if(transfer1 > 0){
				nowPrinting(nowPrint);
			}
		}
		if(nowPrint2.isEmpty() && nowPrint2.size()<1){
			if(!queue.isEmpty()){
				transfer2 = (int) queue.peek();
				queue.poll();
				nowPrint2.offer(transfer2);
				if(transfer2 > 0){
					nowPrinting2(nowPrint2);
				}
			}
		  }
		if(nowPrint3.isEmpty() && nowPrint3.size()<1){
			if(!queue.isEmpty()){
				transfer3 = (int) queue.peek();
				queue.poll();
				nowPrint3.offer(transfer3);
				if(transfer3 > 0){
					nowPrinting3(nowPrint3);
				}
			}
		  }
	  }
	}

	// now printing printers
	public static double nowPrinting(Queue page){
		
		double pages = (int) page.poll();
		
		double timeToPrint;
		timeToPrint = (pages / 10);
		
		System.out.println("Printing on Printer 1:");
		// pauses for n seconds depending on job size
		try {
			System.out.println("Printing... will take about: "+timeToPrint + " seconds" );
			TimeUnit.SECONDS.sleep((long) timeToPrint);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Print Pages: " + pages+ "  Total Time to Print: "+timeToPrint+" seconds");
		
		return timeToPrint;
	}
	public static double nowPrinting2(Queue page){
		
		double pages = (int) page.poll();
		double timeToPrint;
		timeToPrint = (pages / 10);
		System.out.println("Printing on Printer 2:");
		System.out.println("Print Pages: " + pages+ "  Total Time to Print: "+timeToPrint+" seconds");
		return timeToPrint;
	}
	public static double nowPrinting3(Queue page){
		
		double pages = (int) page.poll();
		double timeToPrint;
		timeToPrint = (pages / 10);
		setTime(timeToPrint);
		System.out.println("Printing on Printer 3:");
		System.out.println("Print Pages: " + pages+ "  Total Time to Print: "+timeToPrint+" seconds");
		
		return timeToPrint;
	}
	public static void setTime(double timeToPrint){
		totalTime += timeToPrint;
		
	}
	public static  double getTime(){
		return totalTime;
	}
	
}//class for adding jobs to your queue
		class adding extends TimerTask {
			Random num = new Random();
			int random;
			Queue<Integer> queue;
			int counter = 0;
		    public int getCounter() {
				return counter;
			}
			public void setCounter(int counter) {
				this.counter = counter;
			}
			public void setOffering() {
				random = num.nextInt(3)+1;
			}
			public int getOffer(){
				return random;
			}
			public void run() {
		       counter++;
		       System.out.println("Job added");
		       System.out.println(counter);
		       
		       if(counter == 3){
		    	   System.out.println("Done adding jobs");
		       }
		    }
		    
		   
		   
	}

