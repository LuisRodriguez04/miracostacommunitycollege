package edu.miracosta.cs113.hw7;
import java.util.ArrayList;
import java.util.Scanner;

public class SubSetSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Scanner
		Scanner in = new Scanner(System.in);
		//List
		ArrayList<Integer> ints = new ArrayList<Integer>();
		ArrayList<Integer> numUsed = new ArrayList<Integer>();
		ints.add(3);
		ints.add(5);
		ints.add(10);
		ints.add(6);
		ints.add(8);
		ints.add(9);
		ints.add(15);
		ints.add(1);
		ints.add(7);
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("	You will pick a number and that will be your sum."
				+ "\nAnd there will be a list which which will be searched through to see if any "
				+ "\n    of the numbers added up equal to the number you picked.");
		System.out.println("");
		System.out.println("");
		System.out.println("Enter Your Number: ");
		int sum = in.nextInt();
		System.out.println("");
		System.out.println("Now Searching through list.");
		System.out.println("....");
		System.out.println("....");
		boolean x = recursSubSet(sum, ints,numUsed);
		if(x == true){
			System.out.println("");
			System.out.println("True");
		
		}else{
			System.out.println("False");
		}
	}
	public static boolean recursSubSet(int sum, ArrayList<Integer> ints, ArrayList<Integer> numUsed){
		
		if(sum == 0){
			return true;
		}
		for(int i=0; i < ints.size(); i++){
		   sum = sum - ints.get(i);
		   if(sum < 0){
			  sum = sum + ints.get(i);
		   }
		   ints.remove(i);
		   if(ints.isEmpty() && sum!= 0){
			   return false;
		   }
		   
		   return recursSubSet(sum,ints,numUsed);
		}
		return false;
	}


}
