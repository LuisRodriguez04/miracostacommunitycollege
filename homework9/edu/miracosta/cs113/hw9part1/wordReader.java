package edu.miracosta.cs113.hw9part1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.*;

public class wordReader {
	//hey
	public static void main(String[] args) {
		//System.out.println();
		// TODO Auto-generated method stub
		Scanner key = new Scanner(System.in);
		int linecount = 0;
		Map<String,Integer> freq = new HashMap<String,Integer>();
		Map<String,ArrayList<Integer>> line = new HashMap<String,ArrayList<Integer>>();
		System.out.println("Reading file...");
		System.out.println("File_Name = hw9txtWord.txt");
		
		Scanner inputstream = null;
		try{
			inputstream = new Scanner(new FileInputStream("hw9txtWord.txt"));
		}
		catch(FileNotFoundException e){
			System.out.println("File could not be found");
			System.out.println("Exiting program");
			System.exit(0);
		}
		System.out.println("File read....");
		System.out.println();
		System.out.println("____What would you like to do____");
		System.out.println("  1- Read contents of Map");
		System.out.println("  2- Frequency Order(decreasing)");
		System.out.println("  3- Contents based in key value");
		System.out.println();
		System.out.println("Pick Choice: ");
		int choice = key.nextInt();
		System.out.println();
		if(choice ==1){
		  while(inputstream.hasNext()){
			  String word = inputstream.next();
			  wordAndFreq(word, freq,line);
		   }// end of while loop
		  System.out.println("Map created: ");
		  System.out.println("Printing contents....");
		  System.out.println("...");
		  displayMap(freq);
		}// end choice 1 if
		if(choice ==2){
			while(inputstream.hasNext()){
				  String word = inputstream.next();
				  wordAndFreq(word, freq, line);
			   }
			
			System.out.println("Sorted by word frequency");
			ascendSort(freq);
		}
		if(choice == 3){
			while(inputstream.hasNext()){
				  String word = inputstream.next();
				  wordAndFreq(word, freq, line);
			   }
			
			System.out.println("Sorted by word frequency");
			freqSorted(freq);
		}
	}
	// checks if the map is empty
	private static void wordAndFreq(String word , Map<String,Integer> freq, Map<String,ArrayList<Integer>> line){
		boolean tf = true;
		  if(freq.containsKey(word)){
			freq.put(word, freq.get(word) + 1);
			tf = false;
		   }
		if(tf == true || freq.isEmpty()){
			freq.put(word, 1);
		}
		
	}
	// displays the map
	public static void displayMap(Map<String,Integer> freq){
		if(freq.isEmpty()){
			System.out.println("Your Map is empty");
		}
		else{
			System.out.println("Word:" + "     Count:");
			System.out.println("");
			for (String b : freq.keySet()) {
			    System.out.println(b + "     "+freq.get(b));
			}
			
		}
	}//bubble sort least to great
	public static void freqSorted(Map<String,Integer> freq){
		int mapFreq[] = new int[freq.size()];
		int index = 0;
		for (Map.Entry entry : freq.entrySet()) {
		    int v = (int) entry.getValue();
		    mapFreq[index]= v;
		    index++;
		}
		boolean fix = false;
		int temp;
		while(fix == false){
			fix = true;
			for(int i =0;i < mapFreq.length-1;i++){
				if(mapFreq[i] > mapFreq[i+1]){
					temp = mapFreq[i+1];
					mapFreq[i+1] = mapFreq[i];
					mapFreq[i] = temp;
					fix = false;
				}
			}
		}
		for (int i = 0; i < mapFreq.length; i++) {
			   System.out.print( mapFreq[i] + ", ");
		}
		System.out.println("");
		System.out.println("......Done!");
	}
	//bubble greatest to least
	public static void ascendSort(Map<String,Integer> freq){
		int mapFreq[] = new int[freq.size()];
		int index = 0;
		for (Map.Entry entry : freq.entrySet()) {
		    int v = (int) entry.getValue();
		    mapFreq[index]= v;
		    index++;
		}
		boolean fix = false;
		int temp;
		while(fix == false){
			fix = true;
			for(int i =0;i < mapFreq.length-1;i++){
				if(mapFreq[i] < mapFreq[i+1]){
					temp = mapFreq[i+1];
					mapFreq[i+1] = mapFreq[i];
					mapFreq[i] = temp;
					fix = false;
				}
			}
		}
		for (int i = 0; i < mapFreq.length; i++) {
			   System.out.print( mapFreq[i] + ", ");
		}
		System.out.println("");
		System.out.println("......Done!");
	}
	
}
