package edu.miracosta.cs113.FinalProject;

public class movieReader {
	String name;
	String rating;
	String amount;
	String star;
	String pop;
	public String getName() {
		return name;
	}
	public void setName(String newname) {
		this.name = newname;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String newrating) {
		this.rating = newrating;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String newamount) {
		this.amount = newamount;
	}
	public String getStar(){
		return star;
	}
	public void setStar(String newstar){
		this.star = newstar;
	}
	public void setPop(String newPop){
		this.pop = newPop;
	}
	public String getPop(){
		return pop;
	}
	public String toString(){
		return "Movie Name:  "+   name    + "     " + "ESRB: " +rating + "   Rating: " + star+ "  Stars"; 
	}
	public String toString2(){
		return "Movie Name:  "+   name    + "     " + "ESRB: " +rating + "   Rating: " + star+ "  Stars" + " Time rented:  " + pop; 
	}
}
