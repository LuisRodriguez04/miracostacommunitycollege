package edu.miracosta.cs113.FinalProject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.RootPaneContainer;

public class movieClass extends JFrame{
	public int width = 1000;
	public int length = 750;
	int i ,s = 0;
	double mvprice = .5;
	int shake[];
	public String[] wrds;
	public String word;
	private JMenuBar menu;
	private JMenuBar sMenu;
	private JMenu sortRate,sortStars,sortPop;
	private JMenuItem movieItem, myListitem ,gohome;
	private JMenuItem Ritem, pgItem, pg13Item,htol,ltoh,sortP;
	private JMenu myList, movies, home;
	private JPanel pnl, picpnl2, p,picpnl, rpnl,pgpnl,p13pnl ,low,high;
	private JPanel sPnl, foundMovie, listpnl,pay,fnl;
	public JPanel homep = new JPanel();;
	public JLayeredPane layeredPane;
	public JLabel user, pw, mvlbl,card , finaltext, finaltext2,finaltext3, ttl;
	public JTextField ufield, pfield, searchfield,rentfield, removefield,cardinfo;
	public JButton login, create, search,go, rentButton, remove ,addlist, checkout, finish,finalbutton;
	public JCheckBox rent;
	JScrollPane scrollFrame;
	Font font = new Font("Courier", Font.ITALIC, 20);
	String readline, movie, rating, amount,stars,popu;
	movieReader obj = new movieReader();
	Map<String,ArrayList<String>> esrb = new HashMap<String,ArrayList<String>>();
	Map<Integer,ArrayList<String>> sRating = new HashMap<Integer,ArrayList<String>>();
	Map<Integer,String> sorting = new HashMap<Integer,String>();
	Map<String,String> movieSearch = new HashMap<String,String>();
	Map<String,JCheckBox> rentClick = new HashMap<String,JCheckBox>();
	ArrayList<String> r1;
	ArrayList<String> p13;
	ArrayList<String> pmap;
	ArrayList<String> sRate;
	ArrayList<String> mList;
	ArrayList<String> alist = new ArrayList<String>();
	String[] atHome = new String[3];
	String[] atList = new String[30];
	public movieClass(){
		super("Welcome to Movie Store");
		pnl = new JPanel();
		pnl(pnl);
		rpnl = new JPanel();
		picpnl = new JPanel();
		picpnl2 = new JPanel();
		pgpnl = new JPanel();
		p13pnl = new JPanel();
		foundMovie = new JPanel();
		p = new JPanel();
		sPnl = new JPanel();
		low= new JPanel();
		high= new JPanel();
		listpnl = new JPanel();
		p.setLayout(new BorderLayout());
		menu = new JMenuBar();
		sMenu = new JMenuBar();
		searchPnl();
		readMovies(pnl);
		setSize(getWidth(),getHeight());
		picpnl.add(new JLabel( new ImageIcon( "flix.jpg")));
		picpnl2.add(new JLabel( new ImageIcon( "flix2.jpg")));
		myList = new JMenu("My List");
		movies = new JMenu("Movies");
		home = new JMenu("To Home");
		sortStars = new JMenu("Sort by Rating");
		sortRate = new JMenu("Sort by ESRB Rating");
		sortPop = new JMenu("Sort by Times Rented");
		checkout = new JButton("Checkout");
		start();
		pack();
	}//End Of Constructor
	public JPanel pnl (JPanel pnl){
		pnl.setLayout(new GridLayout(0,1));
		return pnl;
	}
	public void start(){
		Bar(menu);
		inBar(sortRate);
		sortM(sMenu);
		add(p,BorderLayout.CENTER);
		add(picpnl, BorderLayout.WEST);
		add(picpnl2,BorderLayout.SOUTH);
	}
	private JMenuBar Bar(JMenuBar menu){
		menu.add(movies);
		menu.add(myList);
		menu.add(home);
		setJMenuBar(menu);
		menuBar(menu,movies,myList,home);
		add(menu, BorderLayout.NORTH);
		return menu;
	}
	private JMenuBar menuBar(JMenuBar menu, JMenu movies, JMenu myList, JMenu home){
		movieItem = new JMenuItem("Movie List");
		movies.add(movieItem);
		myListitem = new JMenuItem("Go to...");
		myList.add(myListitem);
		gohome = new JMenuItem("Go Home");
		home.add(gohome);
		movieItem.addActionListener(new LineListener());
		gohome.addActionListener(new LineListener());
		myListitem.addActionListener(new LineListener());
		return menu;
	}
	private JMenuBar sortM(JMenuBar sort){
		sort.add(sortRate);
		sort.add(sortStars);
		setJMenuBar(sort);
		p.add(sort, BorderLayout.NORTH);
		return sort;
	}
	private JMenu inBar(JMenu sortRate){
		Ritem = new JMenuItem("R");
		pg13Item = new JMenuItem("Pg-13");
		pgItem = new JMenuItem("PG");
		htol = new JMenuItem("High to Low");
		ltoh = new JMenuItem("Low to High");
		sortP = new JMenuItem("Sort...");
		sortRate.add(Ritem);
		sortRate.add(pg13Item);
		sortRate.add(pgItem);
		sortStars.add(htol);
		sortStars.add(ltoh);
		sortPop.add(sortP);
		
		Ritem.addActionListener(new LineListener());
		pg13Item.addActionListener(new LineListener());
		pgItem.addActionListener(new LineListener());
		htol.addActionListener(new LineListener());
		ltoh.addActionListener(new LineListener());
		go.addActionListener(new LineListener());
		addlist.addActionListener(new LineListener());
		search.addActionListener(new ButtonListener());
		sortP.addActionListener(new LineListener());
		return sortRate;
	}
	private JPanel readMovies(JPanel pnl){
		
		
        try {
            BufferedReader file = new BufferedReader(new FileReader(new File("loginInfo.txt")));
            readline = file.readLine();
            readline = file.readLine();
            while ((readline = file.readLine()) != null) {
                if (readline.contains("|")) {
                   
                    readline = readline.trim();
                    
                    String[] read = readline.split("[|]");
              
                    movie = read[1].trim();
                    JLabel mv = new JLabel(movie);
                    obj.setName(movie);
                    rating = read[2].trim();
                    rating.trim().toUpperCase();
                    JLabel rt = new JLabel(rating);
                    obj.setRating(rating);
                    amount = read[3].trim();
                    JLabel am = new JLabel(amount);
                    obj.setAmount(amount);
                    stars = read[4].trim();
                    JLabel st = new JLabel(stars);
                    obj.setStar(stars);
                    popu = read[5].trim();
                    obj.setPop(popu);
                    JLabel toS = new JLabel(obj.toString());
                    toS.setFont(font);
                    pnl.add(toS);
                    searchMovie();
                    sortEsrb();
                    starRating();
                  //  sortPopu();
                }
            }
            file.close();
        } catch (Exception e) {
            System.out.println("errorfile.");
        }
        
        pnl.setVisible(true);
        p.add(pnl, BorderLayout.CENTER);
		return pnl;
	}
	public void sortEsrb(){
	
		if(obj.getRating().equals("R")){
			
			if(!esrb.containsKey("R")){
				r1 = new ArrayList<String>();
				r1.add(obj.toString());
				esrb.put("R", r1);
			}
			else{
				esrb.get("R").add(obj.toString());
			}
		}
		else if(obj.getRating().equals("pg13")){
			if(!esrb.containsKey("pg13")){
			    p13 = new ArrayList<String>();
				p13.add(obj.toString());
				esrb.put("pg13", p13);
			}
			else{
			esrb.get("pg13").add(obj.toString());
			}
		}
		else if(obj.getRating().equals("pg")){
			if(!esrb.containsKey("pg")){
				pmap = new ArrayList<String>();
				pmap.add(obj.toString());
				esrb.put("pg", pmap);
			}
			else{
				esrb.get("pg").add(obj.toString());
			}
		}
		
	}
	public void sortPopu(){
		int pops = Integer.parseInt(obj.getPop());
		sorting.put(pops, obj.toString2());
		
	}
	public void sortPn(){
		shake= new int[sorting.size()];
		for ( Integer key : sorting.keySet() ){
			shake[s] = key;
		}
	}
	public void rp(){
		rpnl = new JPanel();
		rpnl.setLayout(new GridLayout(0,1));
		for(String rp : r1){
			JLabel ratedr = new JLabel(rp);
			ratedr.setFont(font);
			rpnl.add(ratedr);
		}
		rpnl.setVisible(true);
		p.add(rpnl, BorderLayout.CENTER);
	}
	public void pgPanel(){
		pgpnl.setLayout(new GridLayout(0,1));
		for(String pg : pmap){
			JLabel ratedpg = new JLabel(pg);
			ratedpg.setFont(font);
			pgpnl.add(ratedpg);
		}
		pgpnl.setVisible(true);
		p.add(pgpnl, BorderLayout.CENTER);
	}
	public void pg13Panel(){
		p13pnl.setLayout(new GridLayout(0,1));
		for(String pg13 : p13){
			JLabel ratedpg13 = new JLabel(pg13);
			ratedpg13.setFont(font);
			p13pnl.add(ratedpg13);
		}
		p13pnl.setVisible(true);
		p.add(p13pnl, BorderLayout.CENTER);
	}
	public void starRating(){
		int stars = Integer.parseInt(obj.getStar());
		if(stars == 5){
			if(!sRating.containsKey(5)){
				sRate = new ArrayList<String>();
				sRate.add(obj.toString());
				sRating.put(5, sRate);
			}
			else{
				sRating.get(5).add(obj.toString());
			}
		}
		else if(stars == 4){
			if(!sRating.containsKey(4)){
				sRate = new ArrayList<String>();
				sRate.add(obj.toString());
				sRating.put(4, sRate);
			}
			else{
				sRating.get(4).add(obj.toString());
			}
		}
        else if(stars == 3){
        	if(!sRating.containsKey(3)){
				sRate = new ArrayList<String>();
				sRate.add(obj.toString());
				sRating.put(3, sRate);
			}
			else{
				sRating.get(3).add(obj.toString());
			}
		}
        else if(stars == 2){
        	if(!sRating.containsKey(2)){
				sRate = new ArrayList<String>();
				sRate.add(obj.toString());
				sRating.put(2, sRate);
			}
			else{
				sRating.get(2).add(obj.toString());
			}
		}
        else{
        	if(!sRating.containsKey(1)){
				sRate = new ArrayList<String>();
				sRate.add(obj.toString());
				sRating.put(1, sRate);
			}
			else{
				sRating.get(1).add(obj.toString());
			}
		}
        
	}
	public void searchPnl(){
		sPnl.setLayout(new GridLayout(0,10));
		JPanel emp = new JPanel();
		JPanel emp2 = new JPanel();
		search = new JButton("Search");
		addlist = new JButton("Add List");
		searchfield = new JTextField(100);
		go = new JButton("Rent");
		rentfield = new JTextField(100);
		sPnl.add(search);
		sPnl.add(searchfield);
		sPnl.add(emp);
		sPnl.add(go);
		sPnl.add(rentfield);
		sPnl.add(addlist);
		sPnl.add(emp2);
		sPnl.setVisible(true);
		p.add(sPnl, BorderLayout.SOUTH);
	}
	public void searchMovie(){
			
			if(!movieSearch.containsKey(obj.getName())){
				movieSearch.put(obj.getName(), obj.toString());
			}
	}
	public void movieFound(){
		
		for ( String key : movieSearch.keySet() ) {
			key.toLowerCase();
			if(searchfield.getText().equals(key)){
				    String found = movieSearch.get(key);
					JLabel urMovie = new JLabel(found);
					foundMovie.add(urMovie);
					urMovie.setFont(font);
					foundMovie.setVisible(true);
					p.add(foundMovie, BorderLayout.CENTER);	
			}
			
		}
	}
	
	public void myHome(){
		homep.setLayout(new GridLayout(0,1));
		for ( String key : movieSearch.keySet() ) {
			key.toLowerCase();
			if(rentfield.getText().equals(key)){
				    String found = movieSearch.get(key);
				    if(i<=2){
	                atHome[i] = found; 
	                String disp = atHome[i];
	                JLabel hlbl = new JLabel(disp);
	                homep.add(hlbl);
	                
	                i++;
				    }
				    else{
						JOptionPane.showMessageDialog(null, "Only 3 movies at home, cant rent more.");
					}
			}
			
		}
	}
	public void disphome(){
		homep.setVisible(true);
		checkout.setVisible(true);
		checkout.addActionListener(new LineListener());
		p.add(homep, BorderLayout.CENTER);
		p.add(checkout, BorderLayout.EAST);
	}
	public void myList(){
		listpnl.setLayout(new GridLayout(0,1));
		for ( String key : movieSearch.keySet() ) {
			key.toLowerCase();
			if(rentfield.getText().equals(key)){
				    String found = movieSearch.get(key);
				    if(i<=30){
	                atList[i] = found; 
	                String disp = atList[i];
	                JLabel hlbl = new JLabel(disp);
	                listpnl.add(hlbl);
	                i++;
				    }
				    else{
						JOptionPane.showMessageDialog(null, "Only 30 movies allowed in list");
					}
			}
			
			
		}
	   }
	public void displist(){
		p.add(listpnl, BorderLayout.CENTER);
		listpnl.setVisible(true);
	}

	public void starlow(){
		high.setVisible(false);
		low= new JPanel();
		low.setLayout(new GridLayout(0,1));
		ArrayList<String> one = sRating.get(1);
		for(String o : one){
			JLabel ones = new JLabel(o);
			ones.setFont(font);
			low.add(ones);
		}
		ArrayList<String> two = sRating.get(2);
		for(String t : two){
			JLabel twos = new JLabel(t);
			twos.setFont(font);
			low.add(twos);
		}
		ArrayList<String> three = sRating.get(3);
		for(String th : three){
			JLabel threes = new JLabel(th);
			threes.setFont(font);
			low.add(threes);
		}
		ArrayList<String> four = sRating.get(4);
		for(String f : four){
			JLabel fours = new JLabel(f);
			fours.setFont(font);
			low.add(fours);
		}
		ArrayList<String> five = sRating.get(5);
		for(String f : five){
			JLabel fives = new JLabel(f);
			fives.setFont(font);
			low.add(fives);
		}
		low.setVisible(true);
		p.add(low, BorderLayout.CENTER);
		
	}
    public void starhigh(){
    	high= new JPanel();
		high.setLayout(new GridLayout(0,1));
    	ArrayList<String> five = sRating.get(5);
		for(String f : five){
			JLabel fives = new JLabel(f);
			fives.setFont(font);
			high.add(fives);
		}
		ArrayList<String> four = sRating.get(4);
		for(String f : four){
			JLabel fours = new JLabel(f);
			fours.setFont(font);
			high.add(fours);
		}
		ArrayList<String> three = sRating.get(3);
		for(String th : three){
			JLabel threes = new JLabel(th);
			threes.setFont(font);
			high.add(threes);
		}
		ArrayList<String> two = sRating.get(2);
		for(String t : two){
			JLabel twos = new JLabel(t);
			twos.setFont(font);
			high.add(twos);
		}
		ArrayList<String> one = sRating.get(1);
		for(String o : one){
			JLabel ones = new JLabel(o);
			ones.setFont(font);
			high.add(ones);
		}
		p.add(high, BorderLayout.CENTER);
	}
 public void paypnl(){
	 pay = new JPanel();
	 pay.setLayout(new GridBagLayout());
	 card = new JLabel("Credit Card:   ");
	 card.setFont(font);
	 cardinfo = new JTextField(10);
	 finish = new JButton("Finish");
	 finish.addActionListener(new LineListener());
	 pay.add(card);
	 pay.add(cardinfo);
	 pay.add(finish);
	 pay.setVisible(true);
	 p.add(pay, BorderLayout.CENTER);
	 
 }
 public void finalpnl(){
	 fnl = new JPanel();
	 double total = mvprice * i;
	 String total2 = "$ "+  Double.toString(total) + "   ";
	 fnl.setLayout(new GridBagLayout());
	 finaltext = new JLabel("Your card was approved!");
	 finaltext2 = new JLabel("Thank You Come again!");
	 finaltext3 = new JLabel("Your Total: ");
	 JLabel price = new JLabel(total2);
	 finalbutton = new JButton("Exit");
	 finaltext.setFont(font);
	 finaltext2.setFont(font);
	 finaltext3.setFont(font);
	 price.setFont(font);
	 finalbutton.addActionListener(new LineListener());
	 fnl.add(finaltext);
	 fnl.add(finaltext2);
	 fnl.add(finaltext3);
	 fnl.add(price);
	 fnl.add(finalbutton);
	 
	 fnl.setVisible(true);
	 p.add(fnl, BorderLayout.CENTER);
 }
	private class LineListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == Ritem){
				p13pnl.setVisible(false);
			    pnl.setVisible(false);
			    high.setVisible(false);
				low.setVisible(false);
				pgpnl.setVisible(false);
				homep.setVisible(false);
				foundMovie.setVisible(false);
				listpnl.setVisible(false);
				checkout.setVisible(false);
				rp();
			}
			if(e.getSource() == pgItem){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
			    pnl.setVisible(false);
			    high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				foundMovie.setVisible(false);
				listpnl.setVisible(false);
				checkout.setVisible(false);
				pgPanel();
			}
			if(e.getSource() == pg13Item){
				rpnl.setVisible(false);
				pgpnl.setVisible(false);
			    pnl.setVisible(false);
			    high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				foundMovie.setVisible(false);
				listpnl.setVisible(false);
				checkout.setVisible(false);
				pg13Panel();
			}
			if(e.getSource() == movieItem){
				pgpnl.setVisible(false);
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				listpnl.setVisible(false);
			    pnl.setVisible(true);
			    foundMovie.setVisible(false);
			    checkout.setVisible(false);
			}
			if(e.getSource() == htol){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
			    pnl.setVisible(false);
			    pgpnl.setVisible(false);
			    homep.setVisible(false);
			    low.setVisible(false);
			    high.setVisible(true);
			    listpnl.setVisible(false);
			    foundMovie.setVisible(false);
			    checkout.setVisible(false);
			    starhigh();
			}
			if(e.getSource()==ltoh){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
			    pnl.setVisible(false);
			    pgpnl.setVisible(false);
			    homep.setVisible(false);
			    listpnl.setVisible(false);
			    foundMovie.setVisible(false);
			    checkout.setVisible(false);
			    starlow();
			}
			if(e.getSource() == go){
                     myHome();
			}
			if(e.getSource() == gohome ){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				pnl.setVisible(false);
				pgpnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				listpnl.setVisible(false);
				foundMovie.setVisible(false);
				disphome();
			}
			if(e.getSource() == myListitem){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				pnl.setVisible(false);
				pgpnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				listpnl.setVisible(false);
				foundMovie.setVisible(false);
				checkout.setVisible(false);
				displist();
			}
			if(e.getSource() == addlist){
				listpnl.setVisible(true);
				myList();
			}
			if(e.getSource() == checkout){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				pnl.setVisible(false);
				pgpnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				listpnl.setVisible(false);
				foundMovie.setVisible(false);
				listpnl.setVisible(false);
				checkout.setVisible(false);
				paypnl();
			}
			if(e.getSource() == finish){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				pnl.setVisible(false);
				pgpnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				listpnl.setVisible(false);
				foundMovie.setVisible(false);
				checkout.setVisible(false);
				pay.setVisible(false);
				boolean card = true;
				String a = null;
				while(card)
				{
				 // grabs user credit card input
				  cardinfo.getText();
				  a =  cardinfo.getText();
				 // checks if it is all integers numbers
				  try
				  {
				  if(checkInteger(a) == false)
				  {
					  throw new NonNumberException("Error");
				  }
				  }
				  catch(NonNumberException n)
				  {
					  JOptionPane.showMessageDialog(null, n.getMessage());
					  break;
				  }
				  // checks and makes sure length is 8
				  try
				  {
					if (a.length() == 8)
					{
					  card = false;
					  break;
					}
					else
					 throw new LengthException("Error");
				
				  }
				  catch(LengthException c)
				  {
					  JOptionPane.showMessageDialog(null, c.getMessage());
					  break;
				  }
				}
				if(card == false)
				{
	              finalpnl();
				}
			}
			if(e.getSource() == finalbutton){
				System.exit(0);
			}
			
		}
	}
	public boolean checkInteger(String a) 
	{
		char[] c = a.toCharArray();
	      for(int i=0; i < a.length(); i++)
	      {
	          if ( !Character.isDigit(c[i]))
	          {
	             return false;
	          }
	     }
	     return true;
	}
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource()== search){
				rpnl.setVisible(false);
				p13pnl.setVisible(false);
				pnl.setVisible(false);
				pgpnl.setVisible(false);
				high.setVisible(false);
				low.setVisible(false);
				homep.setVisible(false);
				listpnl.setVisible(false);
				checkout.setVisible(false);
				movieFound();
	        }
		}
	}
	public static void shakerShort(int[] shake){
    	boolean swapped = true;
        int i = 0;
        int j = shake.length - 1;
        while(i < j && swapped) 
        {
           swapped = false;
           for(int k = i; k < j; k++) 
           {
              if(shake[k] > shake[k + 1]) 
              {
                 int temp = shake[k];
                 shake[k] = shake[k + 1];
                 shake[k + 1] = temp;
                 swapped = true;
              }
           }
           j--;
           if(swapped) 
           {
              swapped = false;
              for(int k = j; k > i; k--) 
              {
                 if(shake[k] < shake[k - 1]) 
                 {
                    int temp = shake[k];
                    shake[k] = shake[k - 1];
                    shake[k - 1] = temp;
                    swapped = true;
                 }
              }
           }
           i++;
        }
    }
}
