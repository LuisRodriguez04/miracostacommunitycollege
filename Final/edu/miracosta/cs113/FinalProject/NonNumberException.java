package edu.miracosta.cs113.FinalProject;

public class NonNumberException extends Exception{
		public NonNumberException(String message)
		{
			super("Credit card includes invalid character. Try again");	
		}
}
