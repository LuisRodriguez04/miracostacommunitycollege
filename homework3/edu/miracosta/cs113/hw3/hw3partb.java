package edu.miracosta.cs113.hw3;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class hw3partb {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
        LinkedList<String> student = new LinkedList<String>();
        Scanner key = new Scanner(System.in);
        
        PrintWriter outputStream = null;       
        int choice;
        String name;
        boolean exit = true;
        student.add("Sam");
        student.add("Luis");
        student.add("Mand");
        student.add("Ted");
        
        System.out.println("Student Enrollment List");
        System.out.println("");
		System.out.println("1- Add Student to front of list\n"
				          +"2- Remove Student from Beginning of list\n"
				          +"3- Add Student to end of the list\n"
				          +"4- Remove student by name\n"
				          +"5- Print List\n"
				          +"6- Exit");
		System.out.println("");
		 try
	        {
	            outputStream = new PrintWriter(new FileOutputStream("hw3partb.txt",true));
	        }
	        
	        //Check to see if file can be created or exists. If not, end program
	        catch(FileNotFoundException e)
	        {
	            System.out.println("Error opening the file dataFile.");
	            System.exit(0);
	        }
		while(exit == true)
		{
		 System.out.println("Choice:");
		 choice = key.nextInt();
		 System.out.println("");
		 switch(choice)
		 {
		   case 1:
			  System.out.println("Add student:");
			  name = key.next();
			  long startTime = System.nanoTime();
			  student.addFirst(name);
			  long stopTime = System.nanoTime()-startTime;
			  String line = "Adding Student front of list:\n" + "NanoTime: " + stopTime; 
			  outputStream.println(line);
			  System.out.println("Adding Student front of list:\n" + "NanoTime: " + stopTime);
			  break;
		   case 2:
			   long startTime2 = System.nanoTime();
			   student.removeFirst();
			   long stopTime2 = System.nanoTime()-startTime2;
			   String line2 = "Remove Student front of list:\n" + "NanoTime: " + stopTime2; 
			   outputStream.println(line2);
			   System.out.println("Removing Student front of list:\n" + "NanoTime: " + stopTime2);
			   break;
		   case 3:
			   System.out.println("Add student:");
			   name = key.next();
			   long startTime3 = System.nanoTime();
			   student.add(name);
			   long stopTime3 = System.nanoTime()-startTime3;
			   String line3 = "Adding Student end of list:\n" + "NanoTime: " + stopTime3; 
			   outputStream.println(line3);
			   System.out.println("Adding Student end of list:\n" + "NanoTime: " + stopTime3);
			  break;
		   case 4:
			  System.out.println("Enter Name:");
			  name = key.next();
			  long startTime4 = System.nanoTime();
			  for(String element : student)
			  {
				  if(name == element)
				  {
					student.removeFirstOccurrence(element);
				  }
			  }
			  long stopTime4 = System.nanoTime()-startTime4;
			  String line4 = "Remove Student by name:\n" + "NanoTime: " + stopTime4; 
			  outputStream.println(line4);
			  System.out.println("Removing Student by name:\n" + "NanoTime: " + stopTime4);
			  break;
		   case 5:
			   for(String newList : student)
					  System.out.println(newList);
			   break;
		   case 6:
			  exit = false;
			  break;
		   default:
			  System.out.println("Incorrect Choice.");
		 }
		 System.out.println("");
	}
		outputStream.close();
  }
}
