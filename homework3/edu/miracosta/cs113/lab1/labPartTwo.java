package edu.miracosta.cs113.lab1;

import java.util.ArrayList;

public class labPartTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> alist = new ArrayList<String>();
		alist.add("0");
		alist.add("1");
		alist.add("2");
		alist.add("3");
		System.out.println("Old List");
		for(String elem : alist)
		{
		 System.out.println(elem);
		}
		String target = "0";
		delete(alist,target);
		System.out.println("");
		System.out.println("New List");
		for(String elem : alist)
		{
		 System.out.println(elem);
		}
	}
    public static void delete(ArrayList<String> alist, String target)
    {
     int targ = alist.indexOf(target);
     alist.remove(targ);
    }
}
