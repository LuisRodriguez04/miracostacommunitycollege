package edu.miracosta.cs113.hw10;

public interface KWHashMap<K, V> {
    V get(Object key);
    V put(K key, V value);
    V remove(Object key);
    int size();
    boolean isEmpty();
}
