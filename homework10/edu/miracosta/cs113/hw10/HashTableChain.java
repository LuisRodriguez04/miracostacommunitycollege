package edu.miracosta.cs113.hw10;

import java.util.AbstractMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashTableChain <K,V> extends AbstractMap<K, V>implements KWHashMap<K,V>{
	private LinkedList<Entry<K, V>>[] table;
	private int numKeys;
	private static final int capacity = 101;
	private static final double loadthreshold = 3.0;
	// insert entry here
	 private class EntrySet extends java.util.AbstractSet<Map.Entry<K, V>> {
		 /** Return the size of the set. */
		 @Override
		 public int size() {
			 return numKeys;
		 }
		 /** Return an iterator over the set. */
		 @Override
		 public Iterator<Map.Entry<K, V>> iterator() {
			 return new SetIterator();
		 	}
	 	}//end entry set
	 private class SetIterator implements Iterator<Map.Entry<K, V>> {
		 int index = 0;
		 Iterator<Entry<K, V>> localIterator = null;
		 public boolean hasNext() {
			 if (localIterator != null) {
				 if (localIterator.hasNext()) {
					 return true;
				 } else {
					 localIterator = null;
					 index++;
				 }
			 }
			 while (index < table.length
					 && table[index] == null) {
				 index++;
			 }
			 if (index == table.length) {
				 return false;
			 }
			 localIterator = table[index].iterator();
			 return localIterator.hasNext();
		 }
		 public Map.Entry<K, V> next() {
			return localIterator.next();
		 }
		 public void remove() { }
	 }//end setiterator class

	public HashTableChain(){
		table = new LinkedList[capacity];
	}
	public String toString() {
		return null;
		
	}
   // public String toString() {
    	
    //}
    public int size(){
    	return table.length;
    }
    public V remove (K key, V value){
    	int index = key.hashCode() % table.length;
    	if(index < 0)
        {
            index += table.length;
        }
 
        if(table[index] == null)
        {
            return null; //Key is not in the list
        }
        Iterator<Entry<K,V>> iter = this.table[index].iterator();
        while (iter.hasNext()) {
            Entry<K,V> nextItem = iter.next();
            if (nextItem.getKey().equals(key)) {
        	V returnValue = nextItem.getValue();
        	iter.remove();
        	return returnValue;
        	}
        }
        return null;
    }
    private void rehash()
    {
        LinkedList<Entry<K, V>>[] old = table;
 
        table = new LinkedList[2*old.length+1];
 
        numKeys = 0;
 
        for(int i = 0; i < old.length; i++ )
        {
                 if(old[i] != null)
                 {
                	 for (Entry <K,V>nextEntry : old[i])
                	  {
                		 put(nextEntry.getKey(), nextEntry.getValue());
                      }
                 }
         }
     }
	 public java.util.Set<Map.Entry<K, V>> entrySet() {
        return new EntrySet();
    }
}