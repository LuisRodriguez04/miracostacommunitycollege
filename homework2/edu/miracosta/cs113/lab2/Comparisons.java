package edu.miracosta.cs113.lab2;

public class Comparisons 
{
	/**
	 * @param args
	 * 
	 * Self Check:
	 * ----------------------------------------------------------
	 * 1. 
	 * ==============
	 * a. O(n)
	 * b. O(n)
	 * c. O(n^2)
	 * d. O(n^2)
	 * 
	 * 3. 
	 * ================
	 * a. O(log n ) is meant for smaller numbers since when at high n values it will give you a significantly 
	 *    low number.
	 *    f(50) = 5.64
	 * b. O(n) will give you a lacking performance of growth since n will only increase by 1.
	 *    n+5 f(75)= 80 f(90) = 95 after 15 n only have gone up by 15
	 * c. O(n log n ) performs better in getting from 2000 to 4000 since it gives a increase in a good amount with smaller ranges.
	 *    f(50) = 282 f(500)= 1350 but from 4000 to 8000 its less efficient because the range is larger.
	 * d. O(n^2) is great in performance t0 get you from 2000 to 4000 as you can get there fairly quick
	 *   f(50)= 2,500 f(100)= 10,000 as well as 4000 to 8000 is great in performance as you see for f(50) to f(100)
	 *   it goes through 2000 to 8000 plus more.
	 * e. O(n^3) will get you to those high numbers in the matter of steps.
	 *    f(13)= 2197 f(20)=8000 
	 * 
	 * 4.
	 * ========================
	 * for exponential it grows very quickly.
	 * for cubic it grows quickly as well 
	 * for quadratic it grows steadily but increases over time 
	 * for log-linear it will it increase slow and steady 
	 * for linear it increases at a steady pace
	 * for logarithmic it increases very very slowly
	 * 
	 * lab 2: programming 1.
	 * ===============================================================
	 * The answer does not surprise me by looking at the equation
	 * you can see that y2 will increase much faster than y1.
	 * y1 only increases n by 100 each n pass through.
	 * While y2 will increase more than by n squared.
	 * Also by big o notation you can see that n^2 > n 
	 * 
	 */
	public static void main(String[] args) 
	{
	 int y1;
	 int y2;
	 
	 for(int n = 0; n < 100; n= n+9)
	 {
		 y1 = 100 * n + 10;
		 y2 = 5 * n * n + 2;
		 System.out.println("The answer for Y1: " + y1);
		 System.out.println("The answer for Y2: " + y2);
		 System.out.println("");
		 
	 }
	  

	}
}
