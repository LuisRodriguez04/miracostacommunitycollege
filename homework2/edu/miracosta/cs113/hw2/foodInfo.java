package edu.miracosta.cs113.hw2;

public class foodInfo {
   double percentFat;
   double percentProt;
   double percentCarbs;
   
   public String fish()
   {
	double calories = 35.8;
	double fatCal = 6.7;
	double carbs = 0;
	double protein = 29.2;
	
	percentFat = fatCal/calories *100;
	percentProt = protein/calories *100;
	percentCarbs = carbs/calories*100;
	
	return "Calories: "+ calories + "\nPercent Fat: "+ percentFat 
			+ "\nPercent Carbs: " + percentCarbs 
			+  "\nPercent Protein: " + percentProt;
   }
  
}
