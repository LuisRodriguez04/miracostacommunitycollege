package edu.miracosta.cs113.lab1;

public class Employee extends Person
{
	public String department;
	public String title;
	public int hireDate;
	public Employee(String name, int ssn, int age, String gender, int address,
			int phone, String department, String title, int hireDate) 
	{
	 super(name, ssn, age, gender, address, phone);
	 this.department = department;
	 this.title = title;
	 this.hireDate = hireDate;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getHireDate() {
		return hireDate;
	}
	public void setHireDate(int hireDate) {
		this.hireDate = hireDate;
	}
	@Override
	public String toString() {
		return "Employee [department=" + department + ", title=" + title
				+ ", hireDate=" + hireDate + "]";
	}
	
	

}
