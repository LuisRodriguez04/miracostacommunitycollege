package edu.miracosta.cs113.lab1;

public class Student extends Person
{
  public double gpa;
  public String major;
  public int gradDate;
  
public Student(String name, int ssn, int age, String gender, int address,
		int phone ,int gpa, String major, int gradDate) 
{
	super(name, ssn, age, gender,address,phone);
	this.gpa = gpa;
	this.major = major;
	this.gradDate = gradDate;
}
public double getGpa() 
{
	return gpa;
}
public void setGpa(double gpa) 
{
	this.gpa = gpa;
}
public String getMajor() 
{
	return major;
}
public void setMajor(String major) 
{
	this.major = major;
}
public int getGradDate() 
{
	return gradDate;
}
public void setGradDate(int gradDate) 
{
	this.gradDate = gradDate;
}

  

}
