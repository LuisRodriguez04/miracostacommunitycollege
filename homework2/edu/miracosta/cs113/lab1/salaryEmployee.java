package edu.miracosta.cs113.lab1;

public class salaryEmployee extends Employee
{
 public double annualSalary;

public salaryEmployee(String name, int ssn, int age, String gender, int address,
		int phone, double annualSalary,String department, String title, int hireDate) 
{
	super(name, ssn, age, gender, address,
			phone, department, title, hireDate);
	this.annualSalary = annualSalary;
}

public double getAnnualSalary() {
	return annualSalary;
}

public void setAnnualSalary(double annualSalary) {
	this.annualSalary = annualSalary;
}

@Override
public String toString() {
	return "salaryEmployee [annualSalary=" + annualSalary + "]";
}
 
}
