package edu.miracosta.cs113.lab1;

public class hourlyEmployee extends Employee
{
 public double rate;
 public double hoursWorked;
 public double unionDues;
 
public hourlyEmployee(String name, int ssn, int age, String gender, int address,
		int phone, String department, String title, int hireDate, double rate, double hoursWorked, double unionDues) 
{
	super(name, ssn, age, gender, address,
		phone, department, title, hireDate);
	this.rate = rate;
	this.hoursWorked = hoursWorked;
	this.unionDues = unionDues;
}
public double getRate() {
	return rate;
}
public void setRate(double rate) {
	this.rate = rate;
}
public double getHoursWorked() {
	return hoursWorked;
}
public void setHoursWorked(double hoursWorked) {
	this.hoursWorked = hoursWorked;
}
public double getUnionDues() {
	return unionDues;
}
public void setUnionDues(double unionDues) {
	this.unionDues = unionDues;
}
@Override
public String toString() {
	return "hourlyEmployee [rate=" + rate + ", hoursWorked=" + hoursWorked
			+ ", unionDues=" + unionDues + "]";
}

 
}
