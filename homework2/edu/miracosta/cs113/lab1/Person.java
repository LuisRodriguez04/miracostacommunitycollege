package edu.miracosta.cs113.lab1;

public class Person 
{
  public String name;
  public int ssn;
  public int age;
  public String gender;
  public int address;
  public int phone;
  
  public Person(String name, int ssn, int age, String gender, int address,
		int phone) {
	super();
	this.name = name;
	this.ssn = ssn;
	this.age = age;
	this.gender = gender;
	this.address = address;
	this.phone = phone;
}
@Override
public String toString() {
	return "Person [name=" + name + ", ssn=" + ssn + ", age=" + age
			+ ", gender=" + gender + ", address=" + address + ", phone="
			+ phone + "]";
}
 public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getSsn() {
	return ssn;
}
public void setSsn(int ssn) {
	this.ssn = ssn;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public int getAddress() {
	return address;
}
public void setAddress(int address) {
	this.address = address;
}
public int getPhone() {
	return phone;
}
public void setPhone(int phone) {
	this.phone = phone;
}
  
  
}
